function createNewUser() {
  const user = {};
  user.userName = (prompt('Введи свое имя: ', '')).toLowerCase();
  while (user.userName === '') {
    user.userName = prompt('Ну введи же имя: ', '');
  }
  user.userSecondName = prompt('Введи свою фамилию', '');
  while (user.userSecondName === '') {
    user.userSecondName = prompt('Повторяю.Введи фамилию тут: ', '');
  }
  user.birthday = prompt('Введи дату рождения в формате dd.mm.yyyy', '');
  while (user.birthday === '') {
    user.birthday = prompt('Повторяю.Введи дату рождения тут: ', '');
  }
  user.getAge = function () {
    let current_date = new Date();
    let birthday_date = new Date(this.birthday);
    let age = (current_date.getFullYear()) - (birthday_date.getFullYear());
    return age;
  }
  user.getPassword = function () {
    let birthday_date = new Date(this.birthday);
    let birthday_user_year = birthday_date.getFullYear();
    let newLogin = (this.userName.charAt(0).toUpperCase() + this.userSecondName.toLowerCase() + birthday_user_year);
    return newLogin;
  }
  return user;
}
let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());
