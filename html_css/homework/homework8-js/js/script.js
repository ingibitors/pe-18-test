//функция добавляет div перед элементом с определенным id
function inserting_div(IdBefore) {
    let Element_Id_Before = document.getElementById(IdBefore);
    let New_div = document.createElement('div');
    New_div.setAttribute("id", "before_price");
    document.body.insertBefore(New_div, Element_Id_Before);
}

//функция добавляет текст Текущая цена: ${значение из поля ввода} в созданный элемент по Id
function adding_text_inserted() {
    let adding_block= document.getElementById("Div1");
    adding_block.innerHTML='<span id="button-div"><span id="value"></span><button id="open"> &times</button></span>';
    let adding_text_span=document.getElementById("value");
    value.innerHTML='Текущая цена:'+document.getElementById('price').value;
}
//функция удаляет по id элемента при клике на кнопку x
function deleting(){
    document.getElementById("open").addEventListener("click", function(){
        document.getElementById("button-div").remove();
        document.getElementById('price').value=0;
    });
}
//функция удаляет определенный элемент по id, если он существует
function is_div_exist(element_id){
   if (document.getElementById(element_id)==null){
return false;
   }else{
       document.getElementById(element_id).remove();
   }
}
//функция добавляет определенный текст в элемент по id, если он существует
function adding_text_to_el(element_id,text_for_el){
    if (document.getElementById(element_id)==null){
        return false;
    }else{
        let adding_block=document.getElementById(element_id);
        adding_block.innerHTML=text_for_el;
    }
}
//Блок выполнения
price.addEventListener("focusin", () => price.classList.add('focused'));
price.addEventListener("focusout", () => price.classList.remove('focused'));

inserting_div("form");//Создаем DIV перед формой

price.onchange=function () {
    if (document.getElementById("price").value >= 0) {
        adding_text_inserted();//функция добавляет текст Текущая цена: ${значение из поля ввода} в созданный элемент по Id
        deleting();//функция удаляет по id элемента при клике на кнопку x
        price.className='normal';//зеленый ободок при >=0
        adding_text_to_el("text_mistake","");//функция удаляет текст ошибки при значении поля больше 0
    } else {
        adding_text_to_el("text_mistake","Please enter correct price");//функция добавляет текст ошибки при значении поля меньше 0
        price.className='error';//красный ободок при >=0
        is_div_exist("button-div");//функция удаляет элемент со span
    }
}
