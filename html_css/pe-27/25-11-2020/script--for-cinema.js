// поиск уникальных объектов


function returnUniq(arr) {
const obj={};
const uniqValues=[];
arr.foreach(elem=>{
  for (let i=0; i<uniqValues.length;i++){
    if(uniqValues[i]===elem){
      return;
    }
  }
  uniqValues.push(elem);
});
return uniqValues;
}
console.log(returnUniq(10,2,3,45,2,23,543,3,3));

//использование объекты
function returnUniqWithObj(arr) {
  const obj={};
    arr.forEach(elem=>{
          obj[elem]=true;
      });
    const res=[];
    for (let key in obj){
      res.push(key);
    }
  return res;
}
console.log(returnUniqWithObj(10,2,3,45,2,23,543,3,3));
