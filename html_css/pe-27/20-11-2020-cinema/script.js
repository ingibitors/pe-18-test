"use strict";
/*
let date=new Date();
const date1= new Date("December 17,1995 03:24:00");
const date2= new Date("1995-11-17T03:24:00");
const date3= new Date(Date.now());

/!*console.log(date1===date2);
console.log((date1-date2)/1000);*!/

date3.setFullYear("1990");
console.log(date3);

console.log(typeof date3);*/

// строки

/*const str="";
const str2='';

console.log(typeof str);
console.log(typeof str2);*/

// строки изменить нельзя

/*const str="Some text";
console.log(str[0]);

for (let index=0; index<str.length;index++){
  console.log(str[index]);
  //обязательно обратные кавычки
  console.log(`index: ${index}, char: ${str[index]}`);
}*/

/*const str="Some text";
/!*console.log(str[0].toLowerCase());


console.log(str.indexOf("Some"));
console.log(str.lastIndexOf("e"));

console.log("s"==="S");
console.log("s">"S");*!/

for (let index=0; index<str.length;index++){
  console.log(str[index]);
  //обязательно обратные кавычки
  console.log(`index: ${index}, char: ${str[index]}, code: ${str.charCodeAt(index)}`);
}
console.log(str.includes("Some"));

console.log(str.slice());//"Some text"
console.log(str.slice(3));//"e text"
console.log(str.slice(3,6));//"e t"

console.log(str.substr(3,3));//"e t"
console.log(str.substring(3,6));//"e t"

console.log(str.split("e t"));// разделяет по символам e t
console.log(str.split(""));// разделяет по каждому символу из строки
console.log(str.split(" "));// разделяет по пробелу


console.log('метод repeat '+ str.repeat(2));
console.log(str.replace("Some", "Next"));
console.log(str.trim());*/

// повторить функцию repeat
/*function repeatNew(string,times){
  if (typeof(string)==="string"&&
    string!==""&&
    typeof times==="number"&&
    !isNaN(times)
  ){
  let NewStr="";
  for (let index=0; index<times;index++){
    NewStr+=string;//тоже самое что  NewStr=NewStr+str;
  }
  return NewStr;
}else{
  return "error";
  }
}
console.log(repeatNew("Hello ",10));*/

//task 2
/*const str="task 2"
console.log(capitalizeAndDoublify(str));*///TTAASSKK  22
/*function capitalizeAndDoublify(str) {
  let NewStr="";
  for (let index=0; index<str.length;index++){
    NewStr+=str.toUpperCase();//тоже самое что  NewStr=NewStr+str;
  }
}*/
/*function capitalizeAndDoublify(str) {
  let NewStr = "";
  //цыкл for of для передирания массивов
  for (let value of str) {
    console.log(value);
    NewStr += value.repeat(2).toUpperCase();//тоже самое что  NewStr=NewStr+str;
  }
  return NewStr;
}*/
//TASK3
/*const str="very long string very long string very long string";
console.log(truncate(str,15));
function truncate(string,maxLength){
  let dots="...";

  if (string.length>maxLength){
    return string.slice(0,maxLength-dots.length)+dots;
  }
  return str;
}*/

//TASK 4 функция помощник кладовщика
// переводи на новую строчку  \n
/*const products="water,banana,black,tea,apple, cheese";
const store={
  apple:8,
  beef:162,
  banana:14,
  chocolate:0,
  milk:2,
  water:16,
  coffee:0,
  tea:13,
  cheese:0,
};
const storeManHelp=function (product,store) {
let result="";
let productsList=products.split(",");
//проверка на пробелы
for (let index=0; index<productsList.length;index++){
  productsList[index]=productsList[index].trim().toLowerCase();
}

for (const product of productsList){
  if (store.hasOwnProperty(product.toLowerCase())){
    result+=`${product}-${store[product.toLowerCase()]}\n`;
  }else{
    result+=`${product}-"not found"\n`;
  }
}
console.log(result);
};
storeManHelp("water,banana,black,tea,apple,greapefruit",store);*/


//стрелочные функции

/*
const fnName=function () {
  return a+b;
};
let result=fnName(2,"a");//2a

const fnName2=(a,b)=>a+b;//возвращает значение справа от стрелки, но только одну операцию
result=fnName2(2,"a");


const fnName3=(a,b)=>{
  return a+b;
};//если появляются фигурные скобки то нужно писать return
result=fnName3(2,"a");

//стрелочные функции this не передают, т.е их испольщовать нельзя
const user={
  name:"Uasya",
  /!*getName:()=>{
    console.log(this.name);
  },*!/
  get name: function(){

  }
};
user.getName();*/

//все три функции делают одно и тоже
/*function fnOne(a,b) {
  return a+b;
}
const fnTwo=(a,b)=>{
  return a+b;
};
const fnThree=(a,b)=>a+b;
console.log(fnOne(2,3));
console.log(fnTwo(2,3));
console.log(fnThree(2,3));*/

//если нужно задать параметры по умолчанию, то тогда это делается в скобках:
/*

function fnOne(a=3,b=3) {
  return a+b;
}
const fnTwo=(a=3,b=3)=>{
  return a+b;
};
const fnThree=(a=3,b=3)=>a+b;*/


/*function spreadfn1() {
console.log(arguments);
};
//spread оператор .... преобразует данные массива в набор аргументов
const spreadFn=(param1,param2,...rest)=>{
  console.log(param1,param2);
};

const spreadFn=(...rest)=>{
  console.log(...rest);
};
spreadfn1(2,3,"4",null,true);
spreadFn(2,3,"4",null,true);*/

//task 1 функция сумматор через стрелочную фукцию

/*const summ=(a,b)=>a+b;
console.log(summ(5,6));

const summ2=(a,b)=>{return a+b;};
console.log(summ2(5,6));*/

//TASK2

const summ=(...number)=>{
  console.log(number);
  if (number.length<3){
    alert('error');
    return;
  }
}
let result=0;
for (const iterator of number){
    result+=iterator;
}
console.log(result);

for(let index=0;index<number.length;index++){
  const element = number[index];
  if (typeof element !=="number"||isNaN(element)){
    alert(`error! Element ${index+1} not a number`);
  }
}
summ(5,5,10,20,65);
