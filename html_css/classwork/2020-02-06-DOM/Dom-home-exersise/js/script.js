//  https://api-2d3d-cad.com/html5_javascript/ хороша стаття для розбору DOM
//пишем функцию добавления нового пункта в списке
function addLi() {
  var stroka = prompt("Введите название строки:", "");//для ввода названия
  if (stroka){
    var ol = document.getElementById ("spisokst");//находим наш список
    var li = document.createElement("LI");//создаем элемент списка
    ol.appendChild(li);//присваиваем нашему списку новый элемент
    var text = document.createTextNode(stroka);//создаем узел текст
    li.appendChild(text);//присваиваем текст новому пункту списка
  }
}
//пишем функцию удаления пунктов из списка
function deleteLi() {
  var ol = document.getElementById ("spisokst");//находим список
  var lastLi = ol.lastChild;//в переменной храним элемент списка
//проверяем на наличие элемента, исключая пробелы, таб. и коммен.
  while (lastLi && lastLi.nodeType != 1){
    lastLi = lastLi.previousSibling;
  }
  if (lastLi){
    lastLi.parentNode.removeChild(lastLi);//удаляем пункт списка,
//если в списке еще что-то осталось
  }
}
