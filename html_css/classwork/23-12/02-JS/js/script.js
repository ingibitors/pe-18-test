const userYearOfBirth = Number(prompt('Введите год вашего рождения', 0));
const userMonthOfBirth = Number(prompt('Введите номер месяца вашего рождения', 0));
const userDayOfBirth = Number(prompt('Введите дату вашего рождения', 0));
const nowYear = 2019;
const nowMonth = 12;
const nowDay = 23;
if (isNaN(userYearOfBirth) || userYearOfBirth === 0) {
  alert('смотри, что вводишь');
  throw new Error('Ошибка ввода.');
}
if (isNaN(userMonthOfBirth) || userMonthOfBirth === 0) {
  alert('смотри, что вводишь');
  throw new Error('Ошибка ввода.');
}
if (isNaN(userDayOfBirth) || userDayOfBirth === 0) {
  alert('смотри, что вводишь');
  throw new Error('Ошибка ввода.');
}

let resultYear;
let resultMonth;
let resultDay;
if (nowMonth>=userMonthOfBirth && nowDay>=userDayOfBirth){
  resultYear=nowYear-userYearOfBirth;
  resultMonth=nowMonth-userMonthOfBirth;
  resultDay=nowDay-userDayOfBirth;
} else if (nowDay>=userDayOfBirth && nowMonth<userMonthOfBirth){
  resultYear=nowYear-userYearOfBirth-1;
  resultMonth=nowMonth+12-userMonthOfBirth;
  resultDay=nowDay-userDayOfBirth;
}
console.log(resultYear,resultMonth,resultDay);
//Это линейный коментарий
/**
 * fe
 **/
