
/* ЗАДАНИЕ - 3
* Написать функцию, которая будет суммировать ВСЕ числа, которые будут переданы ей в качестве аргументов.
* */
/*const sumNumber=function (...numbs) {
let sum=0;
  for (let i=0; i<numbs.length; i++){
    sum=sum+numbs[i]
  console.log(numbs[i])
}
return sum
}
let result =sumNumber(1, 2, 3, 6, 5)

}*/

/* ЗАДАНИЕ - 4
* Написать СТРЕЛОЧНУЮ функцию, которая выводит переданное ей аргументом сообщение, указанное количество раз.
* Принимает два аргумента - само сообщение и число сколько раз его показать.
* Если первый аргумент(сообщение) не передан - ПО УМОЛЧАНИЮ присвоить этому аргументу - "Empty message"
* Если второй аргумент(количество раз) не передан - ПО УМОЛЧАНИЮ присвоить этому аргументу значение 1.
* */
/*const userFunction = function (UserMessage,counter) {
  for (let i = 0; i < counter; i++) {
    console.log(UserMessage);
  }
}
userFunction('Empty message',5)*/

/* ЗАДАНИЕ - 5
* Написать СТРЕЛОЧНУЮ функцию, которая возвращает максимальный переданый ей аргумент.
* Т.е. функции может быть передано потенциально бесконечное количество аргументов(чисел),
* вернуть нужно самый большой из них.
* */
/*const maxNumb1=(...numbs)=>{
  let maxNumb=Math.max(...numbs);
  return maxNumb;
}
const result2 = maxNumb1 (numbs:1, 27, 55, 676, 776);*/

/*
const fruitShop = {
  items: {
    banana: 5,
    orange: 1,
    apple: 2,
    pineapple: 0,
    wetermelon: 7,
    kivi: 8,
    lemon: 4,
    melon: 0
  },
  getFruit(fruit,value) {
    if (fruit.toLowerCase() in this.items){
      if (this.items[fruit]<value){
        console.error("this fruits is less than you want")
      }
      else{
        this.items[fruit]-= value;
      }
    }
    else {console.error('no such fruits')}

  }
}
fruitShop.getFruit('banana',3)
*/

const cars =[
  {
    name:'Ford',
    price:2300
  },{
    name:'Fiat',
    price:1100
  },{
    name:'Volvo',
    price:5500
  }
],{
  name:'BMW',
  price:5000
}
}
for (let i=0; i<cars.length; i++){
  cars[i]['price']*=2;
}
cars.sort((i,j)=>i.price-j.price)
